var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var username = "ois.seminar";
var password = "ois4fri";
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}
/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    ehrId = "";
    // TODO: Potrebno implementirati
    return ehrId;
}
/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();
    if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    }
    else {
        $.ajax({
            url: baseUrl + "/ehr"
            , type: 'POST'
            , headers: {
                "Authorization": getAuthorization()
            }
            , success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime
                    , lastNames: priimek
                    , dateOfBirth: datumRojstva
                    , additionalInfo: {
                        "ehrId": ehrId
                    }
                };
                $.ajax({
                    url: baseUrl + "/demographics/party"
                    , type: 'POST'
                    , headers: {
                        "Authorization": getAuthorization()
                    }
                    , contentType: 'application/json'
                    , data: JSON.stringify(partyData)
                    , success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").html("<span class='obvestilo " + "label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    }
                    , error: function (err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            }
        });
    }
}
/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
    var ehrId = $("#preberiEHRid").val();
    if (!ehrId || ehrId.trim().length == 0) {
        $("#preberiSporocilo").html("<span class='obvestilo label label-warning " + "fade-in'>Prosim vnesite zahtevan podatek!");
    }
    else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party"
            , type: 'GET'
            , headers: {
                "Authorization": getAuthorization()
            }
            , success: function (data) {
                var party = data.party;
                $("#preberiSporocilo").html("<span class='obvestilo label " + "label-success fade-in'>Bolnik '" + party.firstNames + " " + party.lastNames + "', ki se je rodil '" + party.dateOfBirth + "'.</span>");
            }
            , error: function (err) {
                $("#preberiSporocilo").html("<span class='obvestilo label " + "label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}
/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
    var ehrId = $("#dodajVitalnoEHR").val();
    var datumInUra = $("#dodajVitalnoDatumInUra").val();
    var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
    var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " + "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    }
    else {
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en"
            , "ctx/territory": "SI"
            , "ctx/time": datumInUra
            , "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak
            , "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
            , "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
            ehrId: ehrId
            , templateId: 'Vital Signs'
            , format: 'FLAT'
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve)
            , type: 'POST'
            , contentType: 'application/json'
            , data: JSON.stringify(podatki)
            , headers: {
                "Authorization": getAuthorization()
            }
            , success: function (res) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
            }
            , error: function (err) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}



function preberiMeritveVitalnihZnakov() {
    var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    var tip = $("#preberiTipZaVitalneZnake").val();
    if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " + "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
    }
    else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party"
            , type: 'GET'
            , headers: {
                "Authorization": getAuthorization()
            }
            , success: function (data) {
                var party = data.party;
                $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " + "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames + " " + party.lastNames + "'</b>.</span><br/><br/>");
                if (tip == "sistolični pritisak") {
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure"
                        , type: 'GET'
                        , headers: {
                            "Authorization": getAuthorization()
                        }
                        , success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " + "table-hover'><tr><th class='text-center'>Datum in ura</th>" + "<th class='text-center'><b>Sistolični krvni tlak</b></th></tr>";
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time + "</td><td class='text-center'>" + res[i].systolic + " " + res[i].unit + "</td></tr>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            }
                            else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov!</span>");
                            }
                        }
                        , error: function () {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                }
                else if (tip == "diastolični pritisak") {
                  
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure"
                        , type: 'GET'
                        , headers: {
                            "Authorization": getAuthorization()
                        }
                        , success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " + "table-hover'><tr><th class='text-center'>Datum in ura</th>" + "<th class='text-center'><b>Diastolični krvni tlak</b></th></tr>";
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time + "</td><td class='text-right'>" + res[i].diastolic + " " + res[i].unit + "</td></tr>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                                
                            }
                            else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov!</span>");
                            }
                           }
                        , error: function () {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                }
            }
            , error: function (err) {
                $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}





$(document).ready(function () {
    /**
     * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
     * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
     * padajočega menuja (npr. Pujsa Pepa).
     
     * 
     * 
     */
     
    $('#preberiPredlogoBolnika').change(function () {
        $("#kreirajSporocilo").html("");
        var podatki = $(this).val().split(",");
        $("#kreirajIme").val(podatki[0]);
        $("#kreirajPriimek").val(podatki[1]);
        $("#kreirajDatumRojstva").val(podatki[2]);
    });
    /**
     * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
     * ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
     */
    $('#preberiObstojeciEHR').change(function () {
        $("#preberiSporocilo").html("");
        $("#preberiEHRid").val($(this).val());
    });
    /**
     * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
     * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
     * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
     * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
     */
    $('#preberiObstojeciVitalniZnak').change(function () {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("");
        var podatki = $(this).val().split("|");
        $("#dodajVitalnoEHR").val(podatki[0]);
        $("#dodajVitalnoDatumInUra").val(podatki[1]);
        $("#dodajVitalnoKrvniTlakSistolicni").val(podatki[2]);
        $("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[3]);
        $("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[4]);
    });
    /**
     * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
     * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Ata Smrk, Pujsa Pepa)
     */
    $('#preberiEhrIdZaVitalneZnake').change(function () {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
        $("#rezultatMeritveVitalnihZnakov").html("");
        $("#meritveVitalnihZnakovEHRid").val($(this).val());
    });
    $(function(){
        var map = new L.map("mapaid").setView([
              46.0536077,14.5247825], 15);
              var lay = new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png");
              
              map.addLayer(lay);
    })
});